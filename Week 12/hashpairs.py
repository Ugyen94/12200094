# generate a pair of hash for s to avoid collisions
def computeHash(s):
	p1, m1 = 31, 10**9+7
	p2, m2 = 37, 10**9+9

	hash1, hash2 = 0, 0
	pow1, pow2 = 1, 1

	for c in s:
		cmap = 1 + ord(c) - ord('a') # integer mapping
		hash1 = (hash1 + cmap * pow1) % m1 
		hash2 = (hash2 + camp * pow2) % m2
		pow1 = (pow1 * p1) % m1
		pow2 = (pow2 * p2) % m2
	
	return hash1, hash2

if __name__ == '__main__':
	s1 = "countermand"
	s2 = "furnace"
	print(computeHash(s1))
	print(computeHash(s2))