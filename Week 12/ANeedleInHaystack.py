def KMP(patr, m, s):
	n = len(s)
	pi = [0]*m
	i = 1
	j = 0

	#construct pie array for pattern
	while i < m:
		if patr[i] == patr[j]:
			pi[i] = j+1
			i, j = i+1, j+1
		else:
			if j == 0:
				#pi[i] = 0 #no need to reassign the same value as the array was initially initialized with zero
				i += 1 
			else: 
				j = pi[j-1]
	#print("The pie array is: ", pi)

	#string iteration
	i = 0 #string index
	j = 0 #pattern index
	flag = 0 
	while i < n:
		if s[i] == patr[j]:
			i, j = i+1, j+1
		else:
			if j == 0:
				i += 1
			else:
				j = pi[j-1] #since j starts from 0, j-1 is taken
		if j == m:
			flag = 1
			print("The pattern is found at index: ", i-j)
			j = pi[j-1]
	if flag == 0:
		print()

if __name__ == '__main__':
	while True:
		try:
			m = int(input())
			p = input()
			s = input()
			KMP(p, m, s)
			print()
			
		except(EOFError):
			break

