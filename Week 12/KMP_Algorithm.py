def KMP(s,p):
	m = len(p)
	n = len(s)
	pi = [0]*m
	i = 1
	j = 0

	#construct pie array for pattern
	while i < m:
		if p[i] == p[j]:
			pi[i] = j+1
			i, j = i+1, j+1
		else:
			if j == 0:
				#pi[i] = 0 #no need to reassign the same value as the array was initially initialized with zero
				i += 1 
			else: 
				j = pi[j-1]
	#print("The pie array is: ", pi)

	#string iteration
	i = j = 0
	while i < n:
		if s[i] == p[j]:
			i, j = i+1, j+1
		else:
			if j == 0:
				i += 1
			else:
				j = pi[j-1] #since j starts from 0, j-1 is taken
		if j == m:
			print("The pattern is found at index: ", i-j)
			j = pi[j-1]

if __name__ == '__main__':

	s = "barfoobarfoobarfoobarfoobarfoo"
	p = "foobarfoo"
	
	KMP(s,p)
	

		


	