import collections

#graph represented as adjacency list
class Graph:
	#constructor
	def __init__(self):
		self.adjList = collections.defaultdict(list)

	def add_edge(self, src, dest):
		self.adjList[src].append(dest)
		self.adjList[dest].append(src)

if __name__ == '__main__':
	g = Graph() #creating object
	g.add_edge(0,1)
	g.add_edge(0,3)
	g.add_edge(0,5)
	g.add_edge(1,3)
	g.add_edge(1,2)
	g.add_edge(2,3)
	g.add_edge(3,4)
	g.add_edge(4,5)
	print(g.adjList)