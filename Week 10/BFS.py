import collections

#graph represented as adjacency list
class Graph:
	#constructor
	def __init__(self):
		self.adjList = collections.defaultdict(list)

	def add_edge(self, src, dest):
		self.adjList[src].append(dest)
		self.adjList[dest].append(src)

	#bfs function 
	def bfs(self, src):
		#declare queue with data type dequeue 
		queue = collections.deque()
		queue.append(src)
		#OR Another way
		#queue = collections.deque([src])

		visited = set() #list can also be used but set does not allow repeated value to be added
		#vistied = list()

		while queue:
			node = queue.popleft() #use pop left for deque to get correct answer instead of pop method
			visited.add(node)

			for nbr in self.adjList[node]:
				if nbr not in visited:
					queue.append(nbr)

		print(visited)


if __name__ == '__main__':
	g = Graph() #creating object
	g.add_edge(0,1)
	g.add_edge(0,2)
	g.add_edge(1,3)
	g.add_edge(1,4)
	g.add_edge(2,5)
	g.add_edge(2,6)
	#print(g.adjList)
	g.bfs(0)