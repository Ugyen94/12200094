
from collections import defaultdict

def dfs(src, p, visited = None):
	if visited is None:
		visited = set()

	visited.add(src)

	for nbr in adjList[src]:
		if nbr not in visited:
			dfs(nbr, src, visited)
			break

		elif nbr != p:
			print(src, nbr)


if __name__ == '__main__':
	edges = [[1,2], [1,3], [1,4], [2,3], [3,4]]
	adjList = defaultdict(list)

	for src, dest in edges:
		adjList[src].append(dest)
		adjList[dest].append(src)

	dfs(1,0) #source and parent