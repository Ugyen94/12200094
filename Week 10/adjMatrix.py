import collections

class Graph:
	#constructor
	def __init__(self, vertices):
		self.V = vertices
		self.adjMatrix = [[0]*self.V for i in range(self.V)] #declared and initialized 2D array

	def add_edges(self, src, dest):
		 self.adjMatrix[src][dest] = 1
		 self.adjMatrix[dest][src] = 1

if __name__ == '__main__':
	g = Graph(5) #creating object
	g.add_edges(0,1)
	g.add_edges(0,2)
	g.add_edges(0,3)
	g.add_edges(1,3)
	g.add_edges(1,4)
	g.add_edges(2,3)
	g.add_edges(3,3)
	g.add_edges(3,4)
	print(g.adjMatrix)