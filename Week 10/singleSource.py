#Single Source Type
from collections import defaultdict
from collections import deque


def addEdges(edges):
	adjList = defaultdict(list)

	for src, dest in edges:
		adjList[src].append(dest)
		adjList[dest].append(src)
	#print(adjList)

	return adjList 

def bfs(src, adjList):
	queue = deque([src])
	#queue.append(src)

	visited = set()
	visited.add(src)

	V = len(adjList)
	dist = [0]*V
	#path
	parent = [-1]*V
	parent[src] = src

	while queue:
		node = queue.popleft()
		for nbr in adjList[node]:
			if nbr not in visited:
				queue.append(nbr)
				visited.add(nbr)
				dist[nbr] = dist[node]+1
				parent[nbr] = node

	#print(dist)
	#print(parent)
	dest = 4
	path = []
	path.append(dest)

	while dest != src:
		dest = parent[dest]
		path.append(dest)
	path.reverse()

	print(path)

if __name__ == '__main__':
	edges = [[0,1], [0,3], [0,5], [1,2], [1,3], [2,3], [3,4], [4,5]]
	adjList = addEdges(edges)
	bfs(0, adjList)