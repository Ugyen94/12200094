# construct binary tree having 0-th root index

def build(s, e, node):
	# base case
	if (s == e):
		stree[node] = arr[s]
		return arr[s]
	m = (s+e)//2

	# build left (s, m)
	left = build(s, m, 2*node+1)
	# build right (m+1, e)
	right = build(m+1, e, 2*node+2)
	# move up from leaf and fill parent nodes/current node
	stree[node] = left + right
	return stree[node]


# find range sum
def qSum(s, e, l, r, node):
	# no overlapp
	if l > e or r < s:
		return 0

	# complete overlap
	if l <= s and r >= e:
		return stree[node] 

	# partial overlap
	mid = (s+e)//2
	# left
	left = qSum(s, mid, l, r, 2*node+1)
	# right
	right = qSum(mid+1, e, l, r, 2*node+2)

	return left+right

# update val at pos i
def qUpdate(s, e, node, pos, val):
	#base case
	if s == e:
		stree[node] = val
		return
	#find mid
	mid = (s+e)//2
	if pos <= mid:
		qUpdate(s, mid, 2*node+1, pos, val) # left
	else:
		qUpdate(mid+1, e, 2*node+2, pos, val) # right

	# update ancestors
	stree[node] = stree[2*node+1]+stree[2*node+2]
	
	
def buildTree():
	return build(0, n-1, 0) # root index 0

def querySum(l, r):
	return qSum(0, n-1, l, r, 0)

def queryUpdate(pos, val):
	return qUpdate(0, n-1, 0, pos, val)

if __name__ == '__main__':
	arr = [1, 3, 4, 2, 8, 7, 5, 6]
	n = len(arr)
	stree = [0]*(4*n)
	buildTree()
	# print(stree)
	print(querySum(3,5)) # 17, 
	queryUpdate(3,8)
	print(querySum(3,5)) # 23
	