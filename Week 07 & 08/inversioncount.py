def merge(arr, s, e):
	i = s
	m = (s+e)//2
	k = 0 # temp array index

	temp = [0]*len(arr)
	count = 0 # inversion count

	while(i<m and m<=e):
		if arr[i] < arr[m]:
			temp[k] = arr[i]
			i += 1
		
		else: # a[i] < a[j]
			temp[k] = arr[m]
			count += (m - i + 1) # inversion count
			m += 1
		
		k+=1

	# copy rem elements from left array
	while(i < m):
		temp[k] = arr[i]
		i += 1
		k += 1

	# copy rem elements from right array
	while(m < e):
		temp[k] = arr[m]
		m += 1
		k += 1

	# copy back to original array
	l = 0
	for i in range(s,e+1):
		arr[i] = temp[l]
		l += 1

	return count


def mergeSort(arr, l, r):
	# base case
	if l >= r:
		return 0
	
	# rec case
	m = (l+r)//2
	c1 = mergeSort(arr, l, m) # left
	c2 = mergeSort(arr, m+1, r) # right
	CI = merge(arr, l, r)

	return c1+c2+CI


if __name__ == '__main__':
	arr = [0, 5, 2, 3, 1]
	print(mergeSort(arr, 0, len(arr)-1))