# recursive
def binary(items, item, l, r):
	if r >= l:
	
		mid = (l + r)//2

		if items[mid] == item:
			return mid
		# left
		elif item < items[mid]:
			r = mid - 1
			return binary(items, item, l, r)
		# right
		else:
			l = mid + 1
			return binary(items, item, l, r) 
	else:
		return -1

# iterative
def binary2(arr, item):
	l = 0
	r = len(arr)-1

	while l <= r:
		mid = (l+r)//2
		if arr[mid] == item:
			return mid
		elif item > arr[mid]:
			l = mid+1

		elif item < arr[mid]:
			r = mid-1
			
	return -1


if __name__ == '__main__':
	arr = [2,3,4,5,7,10]
	x = 8
	pos = binary(arr, x, 0, len(arr)-1)
	#pos = binary2(arr, x)
	if pos == -1:
		print("Item doesn't exist")
	else:
		print(pos)