def partition(arr, s, e):
	p = arr[e]
	i = s - 1

	for j in range(s, e):
		if(arr[j] < p):
			i += 1   # imp ?
			arr[i], arr[j] = arr[j], arr[i]

	# swap(arr[i+1], arr[e])
	arr[i+1], arr[e] = arr[e], arr[i+1]
	return arr, i + 1


def quickSort(arr, l, r):
	# base
	if (l>=r):
		return

	# recur case
	arr, p = partition(arr, l, r)

	quickSort(arr, l, p-1)
	quickSort(arr, p+1, r)

	return arr


# 2 way
def quickSort2(nums):
	n = len(nums)
	if n <= 1:
		return nums
	
	pivot = nums[n-1]
	left = []
	right = []

	for i in range(0, n):
		if nums[i] >= pivot:
			right.append(nums[i])
		
		else:
			left.append(nums[i])

	return quickSort2(left)+[pivot]+quickSort2(right)


if __name__ == '__main__':
	arr = [2,4,5,6,1,3]
	n = len(arr) - 1

	# print(quickSort(arr, 0, n))
	print(quickSort2(arr))

