def f(x):
	y = -(x * x) + 3.345231313 * x # math function given
	return y


if __name__ == '__main__':
	l = -100
	r = 100 # like x axis

	while(r - l > 0.00000001):  # can set precision as given
		m1 = (2*l + r)//3
		m2 = (l+2*r)//3

		if (f(m1)>f(m2)):
			r = m2
		else:
			l = m1
	
	print(l, r)