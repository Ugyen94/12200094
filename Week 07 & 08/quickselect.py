def partition(arr, s, e):
	p = arr[e]
	i = s - 1

	for j in range(s, e):
		if(arr[j] < p):
			i += 1   # imp ?
			arr[i], arr[j] = arr[j], arr[i]

	arr[i+1], arr[e] = arr[e], arr[i+1]
	return arr, i + 1


def quickSelect(arr, l, r, k):
	arr, p = partition(arr, l, r)

	if p == k:
		# print(p,k)
		return arr[p]

	elif k<p :
		return quickSelect(arr, l, p-1, k)
	
	else:
		return quickSelect(arr, p+1, r, k)


if __name__ == '__main__':
	arr = [2,4,5,6,1,6]
	n = len(arr)-1
	k = 0

	print(quickSelect(arr, 0, n, k))		